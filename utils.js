const util = require('util');
function getElapsedTimeMs(time){
  var elapsed =  process.hrtime(time);
  return elapsed[0]*1e3 + elapsed[1]/1e6;
}
const fmt = (num) => {
  var str = String(num);
  if(1 === str.length)
    str = "0" + str;
  return str;
}

function createActiveDiv(arg0, arg1, arg2, arg3){
  var scene = arg0;
  if(typeof(arg0) !== "object") {
    scene = {
      activeDivHh: arg0,
      activeDivMm: arg1,
      activeDivSs: arg2,
      activeDivFr: arg3,
    };
  }

  // RIDICULOUS
  return util.format("%s-%s-%s-%s",
    fmt(scene.activeDivHh),
    fmt(scene.activeDivMm),
    fmt(scene.activeDivSs),
    fmt(scene.activeDivFr));
}

function splitActiveDiv(str){
  var tokens = str.split("-");
  if(tokens.length != 4) {
    console.log("Invalid activeDiv ", str)
    return;
  }
  return {
    activeDivFr: tokens[3],
    activeDivSs: tokens[2],
    activeDivMm: tokens[1],
    activeDivHh: tokens[0],
  }
}

module.exports = {
  getElapsedTimeMs: getElapsedTimeMs,
  splitActiveDiv: splitActiveDiv,
  createActiveDiv: createActiveDiv,
};
