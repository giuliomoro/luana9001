# Luana9001 : a MIDI to webserver translator
Receive MIDI messages from a DAW, keeps the state in memory and alsow writes a text file which can be read next time the webserver is started.
The textfile contains info on what DIV should be displayed and how long it should take to fade in/out. The list of divs is stored in a html age-

Uses https://github.com/justinlatimer/node-midi to handle midi from the node webserver app and socket.io to handle client connections.

Responds to messages on midi channel 15: CC1(msb) and CC2(lsb) for scene selection, CC5 to trigger a vibration, CC11 for class selection (unused), CC12 for fade time (expressed in 0.1s , e.g.: a value of 8 is 800ms), CC13 for target delay.
Sending a CC2 triggers the selection of a new scene.
These can be reassigned in parameters.js (sceneControl property of the parameters object).

The *divId* value is computed as MSB*100 + LSB, so that to obtain, e.g.: 1005, you need MSB = 10, LSB = 5.
LSB should always be <= 99, but this is not enforced

### Setup:
clone the repo, checkout the most up-to-date branch and then run

```
git checkout aws 
npm install
```

### Testing:

To run the server in test mode, run 
```
node index.js
```
you can then send MIDI messages to the server either from within your DAW or using a script, e.g.:
```
node outmidi.js
```

### Running:

To run the server in _"production"_ mode, browse to the luana9001 folder and run:
```
pm2 delete all
pm2 --merge-logs --log luana9001.log start index.js
```
this saves the logs in luana9001.log.

To check the server status 
```
pm2 status
```
To watch the log run
```
while [ 0 -eq 0 ]; do tail luana9001.log ; echo ""; sleep 0.6; done;
```

To update the local copy of the repository
```
git pull origin master
pm2 restart all
```
