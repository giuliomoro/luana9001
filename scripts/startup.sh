#!/bin/bash
cd /home/ubuntu/luana9001/
screen -X -S qmidinet quit
screen -X -S outmidi quit
screen -X -S server quit
screen -d -m -S qmidinet qmidinet
screen -d -m -S server bash -c "while sleep 0.5; do node index.js | tee -a ~/server.log; done"
sleep 1
screen -d -m -S outmidi bash -c "while sleep 0.5; do node outmidi.js; done"

