#!/bin/bash

#strict mode enabled
set -e
set -o pipefail

sudo apt-get update
# add node repo
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install git build-essential nodejs libasound-dev qt4-qmake libqt4-dev
sudo setcap 'cap_net_bind_service=+ep' `which node`

wget http://downloads.sourceforge.net/qmidinet/qmidinet-0.4.1.tar.gz && rm -rf qmidinet-0.4.1 && tar -xvzf qmidinet-0.4.1.tar.gz && cd qmidinet-0.4.1 && ./configure --enable-qt4 && make -j && sudo make install 

