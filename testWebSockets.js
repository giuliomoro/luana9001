"use strict";
var utils = require("./utils.js");

var time = process.hrtime();
const assert = require("assert");
const EventEmitter = require('events');
const util = require('util');
var utils = require("./utils.js");
var ioClient = require('socket.io-client');
var par = require("./parameters.js");
var sc = par.sceneControl;
var midi = require('midi');
var connectionCheckMaxTime = 120000;
var numClients = 200;
var nextEvent = new EventEmitter();
var firstDiv = utils.createActiveDiv(99, 1, 2, 3);
var lastDiv = utils.createActiveDiv(99, 4, 5, 6);

function cts(str){
  console.log(util.format("%d", utils.getElapsedTimeMs(time)) + " " + str);
}

function sendScene(scene){
  console.log("Sending", scene);
  // Prepare the MIDI messages

  var o = splitActiveDiv(scene.activeDiv);
  console.log("Sending: " + createActiveDiv(o);

  midiOutput.sendMessage([sc.statusByte, sc.cc.activeDivHh, activeDivHh]);
  midiOutput.sendMessage([sc.statusByte, sc.cc.activeDivMm, activeDivMm]);
  midiOutput.sendMessage([sc.statusByte, sc.cc.activeDivSs, activeDivSs]);
  midiOutput.sendMessage([sc.statusByte, sc.cc.activeDivFr, activeDivFr]);
  return process.hrtime();
}

function assertScenesEqual(scene0, scene1){
  for(var n in scene0){
    if(n !== "delay"){
      assert.equal(scene0[n], scene1[n], n+" is different");
    }
  }
  for(var n in scene1){
    if(n !== "delay"){
      assert.equal(scene0[n], scene1[n], n+" is different");
    }
  }
}

// Set up a new midi output.
var midiPort = 1;
var midiOutput = new midi.output();
midiOutput.openPort(midiPort);
var server = "http://localhost:"+par.webServerPort;
console.log("Using MIDI port "+midiPort+" : "+midiOutput.getPortName(midiPort))
// create a single socket to test that the messages are sent out correctly
var socket = ioClient(server, {forceNew: true});

var scene = {
  activeDiv: firstDiv,
  activeDivClass: 100,
  fadeTime: 200,
  delay: 1000,
}
sendScene(scene);
socket.on("connect",function(){
  console.log("Single socket connected");
});
socket.on("activeDiv",function(str){
  var receivedScene = JSON.parse(str);
  assertScenesEqual(scene, receivedScene)
  socket.disconnect();
});
socket.on("disconnect", function(){
  nextEvent.emit("loadTest");
});

//when done, load test with numClients
var connectionCheckInterval;
var connectionCheckMaxTimeout;
var socks = new Array(numClients);

nextEvent.on("loadTest", function(){
  var counts = new Array(numClients);
  var numConnectedClients = 0;
  function addClient(){
    numConnectedClients++;
    //  console.log(numConnectedClients)
    if(numConnectedClients === numClients){
      cts("All clients connected");
    }
  }
  // create new client connections
  for(var n = 0; n < numClients; n++){
    socks[n] = ioClient (server, {forceNew: true});
    socks[n].on("connect", function(){
      addClient();
    });
    socks[n].on("disconnect", function(n){
      numConnectedClients--;
      cts("client " + n + " disconnected"); 
    }.bind(null, n));
  }

  // wait for all clients to be connected
  connectionCheckInterval= setInterval(function(){
    if(numConnectedClients === numClients){
      // if the number of connected clients matches
      for(var n = 0; n < socks.length; n++){ // check that they are all actually connected
        var socket = socks[n];
        if(socket.connected !== true){
          cts("Not all connected");
          return;
        }
      }
      cts("All connected");
      connectionCheckInterval.close();
      connectionCheckMaxTimeout.close();
      nextEvent.emit("loadTest2");
    }
  }, 1000);

  // if that is not the case ...
  connectionCheckMaxTimeout = setTimeout(function(){
    assert(false, "Some clients are not yet connected after "+connectionCheckMaxTime/1000+"seconds: " + socks.length + " out of " + numClients);
  }, connectionCheckMaxTime);
});

function sendNextScene(){
  if(scene.activeDiv == firstDiv)
    scene.activeDiv = lastDiv;
  else
    scene.activeDiv = firstDiv;
  return sendScene(scene);
}

var sentSceneTime;
var times;
var init = false;
nextEvent.on("loadTest2",function(){
  // add event listener to all the sockets
  if (init === false){
    init = true;
    for(var n = 0; n < socks.length; n++){
      socks[n].on("activeDiv", function(str){
        //  console.log("RECEIVED: "+str);
        var elapsedTime = utils.getElapsedTimeMs(sentSceneTime);
        var receivedScene = JSON.parse(str);
        var a = times.push(elapsedTime + receivedScene.delay);
        //  console.log("Times: "+a)
        //  cts("receivedDelay: " + receivedScene.delay);
        assertScenesEqual(scene, receivedScene);
      });
    }
  }
  // send a new scene
  times = [];
  scene.delay = 450;
  sentSceneTime = sendNextScene();
  // wait for all responses
  var sceneTimeInterval = setInterval(function(){
    if(times.length === socks.length){
      sceneTimeInterval.close();
      var max = 0;
      var min = Infinity;
      for(var n = 0; n < times.length; n++){
        min = min > times[n] ? times[n] : min;
        max = max < times[n] ? times[n] : max;
      }
      console.log("Range of times: " + min + "~" + max);
      setTimeout(function(){
        console.log("starting a new one ")
        nextEvent.emit("loadTest2");
      }, 500);
    } else {
      console.log("Waiting for all the clients to receive a response");
    }
  }, 1000);
});


/*
for(var n = 0; n < numClients; n++){
  counts[n] = 0;
  setTimeout(function(n){
    //  console.log("timeout "+n)
    socks[n] = require('socket.io-client')("http://localhost:"+par.webServerPort,  {forceNew: true});
    socks[n].on('connect', function(n){
      //  console.log("socks["+n+"] connected");
    }.bind(null,n)
    );
    socks[n].on("disconnect", function(n){
      //  console.log("socks["+n+"] disconnected");
    }.bind(null,n));
    socks[n].on("activeDiv", function(n, str){
      var scene = JSON.parse(str);
      setTimeout(function(activeDiv){
        //  console.log(activeDiv+"           ");
        
      }.bind(null, scene.activeDiv), scene.delay);
      counts[n]++;
      //  console.log(str)
      var elapsed =  process.hrtime(time);
      
      //  console.log(n+" "+counts[n]+" "+countMaxCount+" "+elapsed[0] + elapsed[1]/1e9)
      //  if(countMaxCount === numClients) {
        //  var elapsed =  process.hrtime(time);
        //  console.log("elapsed: ", elapsed[0] + elapsed[1]/1e9);
      //  }
    }.bind(null, n));
  }.bind(null, n), n*0.2);
}

var interval = setInterval(function(){
  for(var n = 0; n < socks.length; n++){
    process.stdout.write(counts[n]+" ");
  }
  process.stdout.write("\r");
}, 10);
*/
function cleanup(){
  for(var n = 0; n < socks.length; n++){
    if(typeof(socks[n]) !== "undefined")
      socks[n].disconnect();
  }
  console.log("disconnect");
  setTimeout(function(){
  process.exit(0)}, 150);
}

//  process.on('exit',cleanup);
process.on('SIGINT',function(){
  cts=function(){};
  if(typeof(connectionCheckInterval) !== "undefined")
    connectionCheckInterval.close();
  if(typeof(connectionCheckMaxTimeout) !== "undefined")
    connectionCheckMaxTimeout.close();
  cleanup();
  console.log();
});
