var midi = require('midi');

// Set up a new input.
var input = new midi.input();

// Count the available input ports.
var availablePorts = input.getPortCount();
console.log("There are "+availablePorts+" input ports available:");
// Get the name of a specified input port.
for (var n = 0; n < availablePorts; n++){
  console.log("Port "+n+": "+input.getPortName(n));
}

// Configure a callback.
input.on('message', function(deltaTime, message) {
  // The message is an array of numbers corresponding to the MIDI bytes:
  //   [status, data1, data2]
  // https://www.cs.cf.ac.uk/Dave/Multimedia/node158.html has some helpful
  // information interpreting the messages.
  console.log('m:' + message + ' d:' + deltaTime);
});

// Open the first available input port.
var port = 1;
input.openPort(port);
console.log("Using port "+port+" : "+input.getPortName(port))
// Sysex, timing, and active sensing messages are ignored
// by default. To enable these message types, pass false for
// the appropriate type in the function below.
// Order: (Sysex, Timing, Active Sensing)
// For example if you want to receive only MIDI Clock beats
// you should use
// input.ignoreTypes(true, false, true)
input.ignoreTypes(false, false, false);

// ... receive MIDI messages ...

// Close the port when done.
setTimeout(function(){
  input.closePort();
}, 100000);