var utils = require("./utils.js");
var midi = require('midi');
var fs = require('fs');
var par = require('./parameters.js');
var portOpen = false;
var showTime = new Date("2045-04-02");

var fadeTimeMult = par.sceneControl.fadeTimeMult;
function localLoop(){
	var i = 0;
	// Set up a new output.
	var output = new midi.output();

	// Count the available output ports.
	var availablePorts = output.getPortCount();
	console.log("There are "+availablePorts+" output ports available:");

	// Get the name of a specified output port.
	for (var n = 0; n < availablePorts; n++){
	  console.log("Port "+n+": "+output.getPortName(n));
	}

	var port = 2;
	output.openPort(port);
	console.log("Using port "+port+" : "+output.getPortName(port))
    function update(){
		var date = new Date();
		if(date >= showTime){
			console.log("ShowTime!!!");
			return;
		}
		var obj = JSON.parse(fs.readFileSync('localLoop.json', 'utf8'));
		var localLoopDivs = obj.localLoopDivs;
		var localLoopInterval = obj.localLoopInterval;
		var localLoopFadeTime = obj.localLoopFadeTime;
		var fadeTimeByte = Math.round(localLoopFadeTime / fadeTimeMult);
		var fadeTimeCc = par.sceneControl.cc.fadeTime;
		var statusByte = par.sceneControl.statusByte;
		output.sendMessage([statusByte, fadeTimeCc, fadeTimeByte]);
		var id = localLoopDivs[i++];
		// div MSB
		var o = utils.splitActiveDiv(id);
		console.log("activeDiv:", utils.createActiveDiv(o));
		output.sendMessage([statusByte, par.sceneControl.cc.vibrate, 10]);
		output.sendMessage([statusByte, par.sceneControl.cc.delay, 10]);
		output.sendMessage([statusByte, par.sceneControl.cc.activeDivHh, o.activeDivHh]);
		output.sendMessage([statusByte, par.sceneControl.cc.activeDivMm, o.activeDivMm]);
		output.sendMessage([statusByte, par.sceneControl.cc.activeDivSs, o.activeDivSs]);
		output.sendMessage([statusByte, par.sceneControl.cc.activeDivFr, o.activeDivFr]);
                i = i % localLoopDivs.length;
		setTimeout(update, localLoopInterval);
    }
    update();
}

localLoop()
// Send a MIDI message.
// Close the port when done.
// output.closePort(); 
