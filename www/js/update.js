$.support.cors = true;
var temporaryAttribute='data-use-once';
var activeDiv=0;
var previousDiv;
var previousDivObj=$("#the-non-existing-object");
var activeDivObj;
var fadeTime=0;
var intervalHandle;
var updateMyContentInterval=200; //milliseconds
var ajaxLongPollingTimeout=5000; //milliseconds
var previousDivClass="";
var timestamp=0;
var socket = io();
socket.on('activeDiv', function(str){
  updateMyContent(str);
});

function updateMyContent(str){
  try {
    var data = JSON.parse(str);
  } catch (exception){
    console.log("invalid JSON received: ", str);
    return;
  }
  var _activeDiv = data.activeDiv;
  var _activeDivClass = data.activeDivClass;
  var _fadeTime = data.fadeTime;
  var _delay = data.delay;
  if(typeof(_delay) === "undefined")
    _delay = 0;
  if(typeof(_fadeTime) === "undefined")
    _fadeTime = 0;
  if (previousDiv==_activeDiv) { 
    // document.getElementById("debug").innerHTML+=("return "+activeDiv+" "+previousDiv+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"); 
    //nothing to do
    return;
  }
  var activeDivId = "div"+_activeDiv;
  var activeDivObj=$('#'+activeDivId);
  if(activeDivObj.length===0){ //either the div does not exist, or it has already been removed or it has not been loaded yet
    console.log(activeDivId, " requested, not found");
    $.get("helpers/get-scenes.php?id="+activeDiv,function(data){  //either way, try to load it again
      activeDivObj=$('#div'+activeDiv); 
      if(activeDivObj.length===0){ //check again, so to make sure you do not add two elements with the same id
        $("#cycler").append(data);
        activeDivObj=$('#div'+activeDiv); 
        activeDivObj=$(''); //if it still does not exist
        // (e.g.: server busy or wrong id was sent), upon calling
        // showActiveDiv, the browser will fade out the currently displayed div and 
        // go back to display an empty screen (*WHATEVER IT MEANS)
      }
    });
  }
  setTimeout(function(_activeDiv, _activeDivClass, _fadeTime){
    showDiv(_activeDiv, _activeDivClass, _fadeTime);
  }.bind(null, _activeDiv, _activeDivClass, _fadeTime), _delay);
}

function showDiv(_activeDiv, _activeDivClass, _fadeTime){
  var _activeDivObj=$('#div'+_activeDiv); 
  _activeDivObj.addClass(_activeDivClass); // we redo the query again here, in case the DOM has changed
  _activeDivObj.css({'z-index':2, "display":"block"});
  if(previousDivObj.length!==0){
    previousDivObj.fadeOut(_fadeTime, fadeOutCallback.bind(null, _activeDiv, _activeDivObj, _activeDivClass));
  } else {
    fadeOutCallback(_activeDiv, _activeDivObj, _activeDivClass);
  }
}

function fadeOutCallback(_activeDiv, _activeDivObj, _activeDivClass){
  if(previousDivObj.is("["+temporaryAttribute+"]")){ //if the div is to be used only once, remove it after use
    previousDivObj.remove();
  } else {
    previousDivObj.css({"z-index":1, "display":"none"});
    previousDivObj.removeClass(previousDivClass);
  }
  _activeDivObj.css({'z-index':3, "display":"block"});
  previousDivObj=_activeDivObj;
  previousDivClass=_activeDivClass;
  previousDiv=_activeDiv;
}
