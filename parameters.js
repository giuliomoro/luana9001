var utils = require("./utils.js");
var parameters = {
  sceneControl:{
    statusByte: 0xBA, //control change on Channel 11
    cc: {
      activeDivFr: 1,
      activeDivSs: 2,
      activeDivMm: 3,
      activeDivHh: 4,
      vibrate: 10,
      ring: 12,
      flashlightOn: 14,
      flashlightOff: 15,
      activeDivClass: 100,
      fadeTime: 101,
      delay: 102,
    },
    vibrationDurationMult: 100, //multiplier to get to milliseconds
    fadeTimeMult: 100, //multiplier to get to milliseconds
    delayMult: 10, // multiplier to get to milliseconds
  },
  saveFolder: "save/", //folder where tmp files are saved
  www: __dirname+"/www/", //home of the web server
  webServerAddresses: ["http://127.0.0.1", "http://54.175.183.89"],
  webServerPort: 3000,
  webServerAdminPort: 3001,
  defaultScene: {
    activeDivFr: 0,
    activeDivSs: 0,
    activeDivMm: 0,
    activeDivHh: 0,
    activeDiv: utils.createActiveDiv(0, 0, 0, 0),
    fadeTime: 1000, 
    activeDivClass: 0,
    timestamp: 0,
    delay: 0,
  },
  receiveLocalMidi: true,
  defaultMidiPort: "LIBE",
}
module.exports=parameters;
