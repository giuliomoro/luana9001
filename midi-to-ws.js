"use strict";
var par = require("./parameters.js");
var utils = require("./utils.js");
var sockets = Array();
var midi = require('midi');

module.exports = function midiToWs(arg) {
var midiPortName = par.defaultMidiPort;
if(arg)
  midiPortName = arg;

for(var n in par.webServerAddresses){
	sockets.push(require('socket.io-client')(par.webServerAddresses[n]+":"+par.webServerAdminPort));
}

for(var n in sockets){
	var io = sockets[n];
	io.on("connect", function(io){
		console.log("Connected to webserver: ", io.io.opts.hostname);
		io.emit("activeDiv", "{\"activeDiv\": \"" + par.defaultScene.activeDiv + "\"}");
	}.bind(null, io));
	io.on("message", function(io, event){
		console.log("Received from webserver ", io.io.opts.hostname, ": ", event);
	}.bind(null, io));
}

function emitActiveDiv(scene, socket){
  var time = process.hrtime();
  var activeDiv = utils.createActiveDiv(scene);
  var obj =  {
    activeDiv: activeDiv,
    activeDivClass: scene.activeDivClass,
    fadeTime: scene.fadeTime,
    delay: scene.delay,
  };
  console.log(JSON.stringify(obj));
  socket.emit("activeDiv", JSON.stringify(obj));
}
// default values
var scene = par.defaultScene;

// Close the MIDI port when done.
  //midiIn.closePort();  
// Set up a new midiIn.
var midiIn = new midi.input();
// Count the available midiIn ports.
var availablePorts = midiIn.getPortCount();
// console.log("There are "+availablePorts+" midiIn ports available:");
// Get the name of a specified midiIn port.
var midiPort = -1;
for (var n = 0; n < availablePorts; n++){
  var name = midiIn.getPortName(n);
  if(name.search(midiPortName) >= 0) {
    midiPort = n;
    console.log("Using Port "+n+": "+name);
    break;
  }
}
if (midiPort < 0){
  console.log("Error: port "+midiPortName+" not found");
  return -1;
}
// Open the first available midiIn port.
midiIn.openPort(midiPort);

// Sysex, timing, and active sensing messages are ignored
// by default. To enable these message types, pass false for
// the appropriate type in the function below.
// Order: (Sysex, Timing, Active Sensing)
// For example if you want to receive only MIDI Clock beats
// you should use
// midiIn.ignoreTypes(true, false, true)
midiIn.ignoreTypes(false, false, false);

// Configure a callback.
midiIn.on('message', function(deltaTime, message) {
  // The message is an array of numbers corresponding to the MIDI bytes:
  //   [status, data1, data2]
  // https://www.cs.cf.ac.uk/Dave/Multimedia/node158.html has some helpful
  // information interpreting the messages.
  console.log("MIDI-IN: " + message + " d:" + deltaTime);
  if(message[0] == par.sceneControl.statusByte){ 
    switch (message[1]){
      case par.sceneControl.cc.activeDivFr: //CC 1
        // console.log("activeDivFr")
        scene.activeDivFr = message[2];
        for(var n in sockets)
          emitActiveDiv(scene, sockets[n]);
      break;
      case par.sceneControl.cc.activeDivSs: //CC 2
        // console.log("activeDivSs")
        scene.activeDivSs = message[2];
      break;
      case par.sceneControl.cc.activeDivMm: //CC 3
        // console.log("activeDivMm")
        scene.activeDivMm = message[2];
      break;
      case par.sceneControl.cc.activeDivHh: //CC 4
        // console.log("activeDivHh")
        scene.activeDivHh = message[2];
      break;
      case par.sceneControl.cc.vibrate: //CC 10
        // console.log("vibrationTime")
		var vibrationDuration = par.sceneControl.vibrationDurationMult * message[2];
		for(var n in sockets)
			sockets[n].emit("vibrate", JSON.stringify({duration: vibrationDuration}));
      break;
      case par.sceneControl.cc.ring: //CC 12
		for(var n in sockets)
			sockets[n].emit("ring");
      break;
      case par.sceneControl.cc.flashlightOn: //CC 14
		for(var n in sockets)
			sockets[n].emit("flashlight", "on");
      break;
      case par.sceneControl.cc.flashlightOff: //CC 15
		for(var n in sockets)
			sockets[n].emit("flashlight", "off");
      break;
      case par.sceneControl.cc.activeDivClass: //CC 100
        // console.log("activeDivClassb")
		for(var n in sockets)
			emitActiveDiv(scene, sockets[n]);
      break;
      case par.sceneControl.cc.fadeTime: //CC 101
        // console.log("fadeTime", message[2]);
        scene.fadeTime = message[2] * par.sceneControl.fadeTimeMult; // the value is interpreted and converted to ms
      break;
      case par.sceneControl.cc.delay: //CC 102
        // console.log("delay");
        scene.delay = message[2] * par.sceneControl.delayMult; // the value is interpreted and converted to ms
      break;
    }
    // saveToDisk('scene', scene); // uncommenting this will make it safer to preserve the state
  }
});
return 0;

}
