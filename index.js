"use strict";
var express = require('express');
var app = express();
var cors = require('cors');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var adminHttp = require('http').Server();
var adminIo = require('socket.io')(adminHttp);
var events = require('events');
var eventEmitter = new events.EventEmitter();
var jsonfile = require('jsonfile');
var fs = require('fs')
var path = require("path");
var par = require("./parameters.js");
var utils = require("./utils.js");
var listeningPort = par.webServerPort;
var adminListeningPort = par.webServerAdminPort;

var receiveLocalMidi = par.receiveLocalMidi;
var midiPort;
process.argv.forEach(function (val, index, array){
	if(val === "--tcp-port" && array[index + 1]){
		listeningPort = array[index + 1];
	}
	if(val === "--no-midi"){
		receiveLocalMidi = false;
	}
	if(val === "--midi-port"){
		midiPort = array[index + 1];
	}
})

if(receiveLocalMidi){
	console.log("Listening for local midi");
	var ret = require("./midi-to-ws.js")(midiPort);
	if(ret) {
		process.exit(1);
	}
} else {
	console.log("NOT listening for local midi");
}

var localScene = par.defaultScene;
var adminSockets = new Array();
var connectedAdmins = 0;
adminIo.on("connection", function(socket){
  var date = new Date();
  console.log(date.toString(), " An admin connected from ", socket.conn.remoteAddress);
  connectedAdmins++;
  socket.emit("message", "I am your slave");
  adminSockets.push(socket);
  socket.on("activeDiv", function(event){
	try{
		var date = new Date();
		console.log(date.toString(), "Received from Admin", event);
		var obj = JSON.parse(event);
		for(var i in localScene){
			if(typeof(obj[i]) !== "undefined")
				localScene[i] = obj[i];
		}
		emitActiveDiv(localScene);
	} catch(err) {
		console.log("Received malformed JSON: ", event);
		console.log(err);
	}
  });
  socket.on("vibrate", function(event){
	console.log("Vibrate--------------------------");
	try{
		var date = new Date();
		console.log(date.toString(), " Received from Admin", event);
		var obj = JSON.parse(event);
		if(typeof(obj.duration) !== "undefined"){
			console.log("vibrating: ", obj.duration);
			emitVibrate(obj.duration);
		}
	} catch (err) {
		console.log("Received malformed JSON: ", event);
		console.log(err);
	}
  });
  socket.on("disconnect", function(socket){
    console.log("The admin disconnected from ", socket.conn.remoteAddress);
    connectedAdmins--; 
    for(var n = 0; n < sockets.length; n++){ //remove old clients. TODO: make this thread-safe
      if (socket === sockets[n]){
        sockets.splice(n, 1);
      }
    }
  }.bind(null, socket));
});
adminHttp.listen(adminListeningPort, function(){
  console.log("Admin client listening on *: "+adminListeningPort);
});

function quit(){
  if(typeof("scene") !== "undefined"){
    saveToDisk("scene", localScene, true);
  }
//TODO: close Midi
}

process.on("exit",function (){
  quit();
});

process.on("SIGINT",function(){
  process.exit(0);
})

function makeSaveFilename(name){
  return par.saveFolder + name + ".json";  
}

function loadFromDisk(name, object){
  var filename = makeSaveFilename(name);
  if(fs.existsSync(filename) === false) {
    return;
  }
  var temp = jsonfile.readFileSync(filename, {throws: false});
  // the above returns null on JSON parse error 
  if(temp !== null){
    for(var property in temp){
      object[property] = temp[property];
    }
  }
}

function saveToDisk(name, object, sync){
  var filename = makeSaveFilename(name);
  if(sync === true){
    var err = jsonfile.writeFileSync(filename, object);
    if(err) {
      console.error(err);
    } else {
      console.log("Succesfully written to disk");
    }
    return;
  } else {
    jsonfile.writeFile(filename, object,  function (err){
      if(err) {
        console.error(err);
      }
    });
  }
}

// if file exists, default values above will be replaced by the file content
loadFromDisk('scene', localScene);

// setup :
// check that directories exist, or create
fs.existsSync(par.saveFolder) || fs.mkdirSync(par.saveFolder);
app.use(cors());
app.use(function (req, res, next) {
    var filename = path.basename(req.url);
    var extension = path.extname(filename);
	//console.log(req.connection.remoteAddress+" GET: " + filename);
    next();
});
app.use("/", express.static(par.www));

function getCurrentDelay(time, delay){
  return delay - utils.getElapsedTimeMs(time);
}

function emitVibrate(dur){
  var interval = (1) / sockets.length;
  var obj =  {
	delay: 0,
    duration: dur,
  };
  for(var n in sockets){
    setTimeout(function(socket, n){
	  console.log("emitting vibrate");
      socket.emit("vibrate", JSON.stringify(obj));
    }.bind(null, sockets[n], n), interval * n);
  }
}

function emitActiveDiv(scene, socket){
  var time = process.hrtime();
  //if(activeDiv == 10000)
    //emitVibrate(1000);
  var obj =  {
    activeDiv: scene.activeDiv,
    activeDivClass: scene.activeDivClass,
    fadeTime: scene.fadeTime,
    delay: scene.delay,
  };
  if(typeof(socket) !== "undefined"){ //send immediately to a specific socket
    obj.delay = getCurrentDelay(time, scene.delay);
    socket.emit("activeDiv", JSON.stringify(obj));
    return;
  }
  var interval = (scene.delay*0.9) / sockets.length;
  for(var n in sockets){
    setTimeout(function(socket, n){
      obj.delay = getCurrentDelay(time, scene.delay);
      socket.emit("activeDiv", JSON.stringify(obj));
    }.bind(null, sockets[n], n), interval * n);
  }
  //  io.emit('activeDiv', JSON.stringify(obj)); //this would broadcast the message to everyone, causing a higher network congestion
  var date = new Date();
  console.log(date.toString(), " ", process.pid + " " + connectedClients + " " +JSON.stringify(obj));
}

var sockets = new Array();
var connectedClients = 0;
io.on("connection", function(socket){
  var date = new Date();
  console.log(date.toString(), " a user connected from ", socket.conn.remoteAddress);
  connectedClients++;
  sockets.push(socket);
  emitActiveDiv(localScene, socket);
  socket.emit("updateFromRemote");
  if(localScene)
    socket.emit("activeDiv", JSON.stringify(localScene));
  socket.on("disconnect", function(socket){
    var date = new Date();
    console.log(date.toString(), " user disconnected from ", socket.conn.remoteAddress);
    connectedClients--; 
    for(var n = 0; n < sockets.length; n++){ //remove old clients. TODO: make this thread-safe
      if (socket === sockets[n]){
        sockets.splice(n, 1);
      }
    }
  }.bind(null, socket));
});

http.listen(listeningPort, function(){
  var date = new Date();
  console.log(date.toString(), "listening on *: "+listeningPort);
});

